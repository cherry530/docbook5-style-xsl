Name:     docbook5-style-xsl
Summary:  Norman Walsh's XSL stylesheets for DocBook 5.X
Version:  1.79.2
Release:  9
License:  MIT and MPLv1.1
URL:      https://github.com/gooselinux/docbook5-style-xsl
Source0:  https://github.com/docbook/xslt10-stylesheets/releases/download/release%2F{%version}/docbook-xsl-%{version}.tar.bz2

Requires: xml-common >= 0.6.3-8

Requires(post):   libxml2 >= 2.4.8
Requires(postun): libxml2 >= 2.4.8

BuildArch: noarch

Obsoletes:  %{name}-extensions < %{version}-%{release}
Provides:   %{name}-extensions = %{version}-%{release}

Provides: docbook-xsl-ns = %{version}

%description
These XSL namespace aware stylesheets allow you to transform any
DocBook 5 document to other formats, such as HTML, manpages, FO,
XHMTL and other formats. They are highly customizable. For more
information see W3C page about XSL.

%package_help

%prep
%autosetup -p1 -n docbook-xsl-%{version}
chmod +x epub/bin/dbtoepub

%build

%install
mkdir -p %{buildroot}%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%version
cp -a [[:lower:]]* %{buildroot}%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%version/
cp -a VERSION %{buildroot}%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%version/VERSION.xsl
ln -s VERSION.xsl %{buildroot}%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%version/VERSION
ln -s xsl-ns-stylesheets-%{version} %{buildroot}%{_datadir}/sgml/docbook/xsl-ns-stylesheets
rm -rf %{buildroot}%{_datadir}/sgml/docbook/xsl-ns-stylesheets/install.sh

%post
%{_bindir}/xmlcatalog --noout --add "rewriteSystem" "http://cdn.docbook.org/release/xsl/%{version}" \
 "file://%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}" %{_sysconfdir}/xml/catalog
%{_bindir}/xmlcatalog --noout --add "rewriteURI" "http://cdn.docbook.org/release/xsl/%{version}" \
 "file://%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}" %{_sysconfdir}/xml/catalog
%{_bindir}/xmlcatalog --noout --add "rewriteSystem" "http://cdn.docbook.org/release/xsl/current/" \
 "file://%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}" %{_sysconfdir}/xml/catalog
%{_bindir}/xmlcatalog --noout --add "rewriteURI" "http://cdn.docbook.org/release/xsl/current/" \
 "file://%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}" %{_sysconfdir}/xml/catalog
%{_bindir}/xmlcatalog --noout --add "rewriteSystem" "http://docbook.sourceforge.net/release/xsl-ns/current" \
 "file://%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}" %{_sysconfdir}/xml/catalog
%{_bindir}/xmlcatalog --noout --add "rewriteURI" "http://docbook.sourceforge.net/release/xsl-ns/current" \
 "file://%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}" %{_sysconfdir}/xml/catalog

%postun
if [ "$1" = 0 ]; then
  %{_bindir}/xmlcatalog --noout --del "file://%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}" %{_sysconfdir}/xml/catalog
fi

%files
%doc COPYING
%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}
%{_datadir}/sgml/docbook/xsl-ns-stylesheets
%{_datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}/extensions
%exclude %{_datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}/extensions

%files help
%doc BUGS README TODO NEWS  RELEASE-NOTES.*  extensions/*.txt

%changelog
* Tue Sep 20 2022 yaoxin <yaoxin30@h-parters.com> - 1.79.2-9
- Fix the postun error

* Tue Dec 3 2019 caomeng<caomeng5@huawei.com> -1.79.2-8
- Package init
